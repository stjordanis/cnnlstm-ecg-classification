
import csv
import random
from datetime import datetime, date
import timeit

from sklearn.model_selection import train_test_split
from sklearn.metrics import plot_confusion_matrix

from imblearn.over_sampling import SMOTE

from scipy import signal,fft,ifft
import scipy.io as spio

import numpy as np
from numpy import ones

from matplotlib import pyplot as plt

import pywt


###########################################################################################################################
# read data from database
###########################################################################################################################
start_process=timeit.default_timer()
read_data_start=timeit.default_timer()
classes = [0,1,2,3,4,]
classes_name = ['F','N','S','V','Q']
data = []
input_length = 280
samples = spio.loadmat("data/mitbih_aami.mat")
samples = samples['s2s_mitbih']
values = samples[0]['seg_values']

labels = samples[0]['seg_labels']
num_annots = sum([item.shape[0] for item in values])


###########################################################################################################################
#  add all segments(beats) together
###########################################################################################################################
l_data = 0
for i, item in enumerate(values):
    for itm in item:
        if l_data == num_annots :
            break
        data.append(np.interp(itm[0],(itm[0].min(),itm[0].max()),(0,1)))
        l_data = l_data + 1


###########################################################################################################################
#  add all labels together
###########################################################################################################################
l_lables  = 0
t_lables = []
for i, item in enumerate(labels):
    if len(t_lables)==num_annots:
        break
    item= item[0]
    for lebel in item:
        if l_lables == num_annots:
            break
        t_lables.append(str(lebel))
        l_lables = l_lables + 1

del values

data=np.reshape(data,(-1,280))
data = np.asarray(data)

###########################################################################################################################
# normalize labels
###########################################################################################################################
n_labels=[]

for i,v in enumerate(t_lables):
    if v == 'F':
        n_labels.append(0)
    if v == 'N':
        n_labels.append(1)
    if v == 'S':
        n_labels.append(2)
    if v == 'V':
        n_labels.append(3)
    if v == 'Q':
        n_labels.append(4)

annotation = np.asarray(n_labels)

read_data_end=timeit.default_timer()
print('read data duration : {} seconds'.format(read_data_end-read_data_start))

def class_details(labels):
    details = np.zeros(np.shape(classes))
    for l in labels:
        for i,v in enumerate(classes):
            if v == l :
                details[i] += 1
    return details

details = class_details(annotation)
for i,v in enumerate(classes):
    print('{:^5} : {:^5}'.format(classes_name[i],details[i]))
for i,j in enumerate([17814,0,6,1905,3168]):
     i+=1 
     plt.subplot(2,5,i)
     plt.title(classes_name[annotation[j]])
     plt.plot(data[j],'b')


# exit()
###########################################################################################################################
# wavelet transform process ...
# haar family: haar
# db family: db1, db2, db3, db4, db5, db6, db7, db8, db9, db10, db11, db12, db13, db14, db15, db16, db17, db18, db19, db20, db21, db22, db23, db24, db25, db26, db27, db28, db29, db30, db31, db32, db33, db34, db35, db36, db37, db38
# sym family: sym2, sym3, sym4, sym5, sym6, sym7, sym8, sym9, sym10, sym11, sym12, sym13, sym14, sym15, sym16, sym17, sym18, sym19, sym20
# coif family: coif1, coif2, coif3, coif4, coif5, coif6, coif7, coif8, coif9, coif10, coif11, coif12, coif13, coif14, coif15, coif16, coif17
# bior family: bior1.1, bior1.3, bior1.5, bior2.2, bior2.4, bior2.6, bior2.8, bior3.1, bior3.3, bior3.5, bior3.7, bior3.9, bior4.4, bior5.5, bior6.8
# rbio family: rbio1.1, rbio1.3, rbio1.5, rbio2.2, rbio2.4, rbio2.6, rbio2.8, rbio3.1, rbio3.3, rbio3.5, rbio3.7, rbio3.9, rbio4.4, rbio5.5, rbio6.8
# dmey family: dmey
# gaus family: gaus1, gaus2, gaus3, gaus4, gaus5, gaus6, gaus7, gaus8
# mexh family: mexh
# morl family: morl
# cgau family: cgau1, cgau2, cgau3, cgau4, cgau5, cgau6, cgau7, cgau8
# shan family: shan
# fbsp family: fbsp
# cmor family: cmor
###########################################################################################################################
# for i,j in enumerate([17814,0,6,1905,3168]):
#      i+=1 
#      plt.subplot(5,2,(2*i)-1)
#      plt.title(classes_name[annotation[j]])
#      plt.plot(data[j],'r')

wt_start=timeit.default_timer()
print('wavelet transform process ...')
data1,data2=pywt.dwt(data,'db2')
data=data1
# data1,data2=pywt.dwt(data,'db2')
# data=data1
wt_end=timeit.default_timer()
print('done')
print('Wavelet transform duration : {} seconds'.format(wt_end-wt_start))
input_length = np.shape(data2)[1]
print('input shape {}'.format(input_length))
print('data1 {}'.format(np.shape(data1)))
print('data2 {}'.format(np.shape(data2)))

for d in data:
   d = np.reshape(d,(input_length,1))

for i,j in enumerate([17814,0,6,1905,3168]):
     plt.subplot(2,5,i+6)
     plt.title(classes_name[annotation[j]])
     plt.plot(data[j],'b')
plt.show()

exit(1)


# ###########################################################################################################################
# # Featur selection
# ###########################################################################################################################

# for i,j in enumerate([17814,0,6,1905,3168]):
#      i+=1 
#      plt.subplot(5,2,(2*i)-1)
#      plt.title(classes_name[annotation[j]])
#      plt.plot(data[j],'--')



# for i,j in enumerate([17814,0,6,1905,3168]):
#      plt.subplot(5,2,2*(i+1))
#      plt.title(classes_name[annotation[j]])
#      plt.plot(data[j],'b')
# plt.show()

# exit(1)

###########################################################################################################################
# split data to train and test 9-1 ...
###########################################################################################################################
print('split data to train and test 9-1 ...')
inter_x_train,inter_x_test,inter_y_train,inter_y_test = train_test_split(data,annotation,test_size=0.1,train_size=0.9,shuffle=True)
print("train data")
details = class_details(inter_y_train)
for i,v in enumerate(classes):
    print('{:^5} : {:^5}'.format(classes_name[i],details[i]))

print("test data")
details = class_details(inter_y_test)
for i,v in enumerate(classes):
    print('{:^5} : {:^5}'.format(classes_name[i],details[i]))

###########################################################################################################################
# over sampling proccess ...
###########################################################################################################################
oversampling_start = timeit.default_timer()
print('over sampling proccess ...')
sm=SMOTE(sampling_strategy={0:2000,2:5000})
oversampling_end = timeit.default_timer()
print('oversampling duration : {} seconds'.format(oversampling_end-oversampling_start))
inter_x_train,inter_y_train=sm.fit_resample(inter_x_train,inter_y_train)
print("train data")
details = class_details(inter_y_train)
for i,v in enumerate(classes):
    print('{:^5} : {:^5}'.format(classes_name[i],details[i]))

print("test data")
details = class_details(inter_y_test)
for i,v in enumerate(classes):
    print('{:^5} : {:^5}'.format(classes_name[i],details[i]))


print('shape of : xtrain {} ytrain {} xtest{} ytest{}'.format(np.shape(inter_x_train)
                                                   ,np.shape(inter_y_train)
                                                   ,np.shape(inter_x_test)
                                                   ,np.shape(inter_y_test)))


###########################################################################################################################
# resize samples to prefix size
###########################################################################################################################
inter_x_train = np.reshape(inter_x_train,[-1,1,input_length,1])
inter_x_test = np.reshape(inter_x_test,[-1,1,input_length,1])
inter_y_train = np.reshape(inter_y_train,[-1,1])
inter_y_test = np.reshape(inter_y_test,[-1,1])


print("input length > {}".format(input_length) )


###########################################################################################################################
# build CNN Model
###########################################################################################################################
import tensorflow as tf

model=tf.keras.models.Sequential()

model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Conv1D(filters=100,
                                 kernel_size=(5),
                                 strides=(2),
                                 activation='relu'),
                                 input_shape=(1,input_length,1)))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.BatchNormalization()))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.ReLU()))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.MaxPool1D(pool_size=(3))))


model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Conv1D(filters=90,
                                 kernel_size=(5),
                                 strides=(2),
                                 activation='relu')))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.BatchNormalization()))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.ReLU()))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.MaxPool1D(pool_size=(3))))

model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Conv1D(filters=80,
                                 kernel_size=(3),
                                 strides=(2),
                                 activation='relu')))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.BatchNormalization()))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.ReLU()))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.MaxPool1D(pool_size=(1))))
model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Flatten()))


# model.add(tf.keras.layers.TimeDistributed(model))
model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(60,return_sequences=True)))
model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(60,return_sequences=True)))
model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(60,return_sequences=True)))
model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(60,return_sequences=True)))

# model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(5)))

model.add(tf.keras.layers.Dense(30))
model.add(tf.keras.layers.Dense(15))
model.add(tf.keras.layers.Dense(5,activation='softmax'))

model.summary()

model.compile(loss='sparse_categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy',])


training_start=timeit.default_timer()
history = model.fit(inter_x_train,inter_y_train,validation_split=0.1,epochs=50)



training_end=timeit.default_timer()
print('Training duration : {} seconds'.format(training_end-training_start))

plt.plot(history.history['accuracy'],'-')
plt.plot(history.history['val_accuracy'],'--')
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

loss , acc =model.evaluate(inter_x_test,inter_y_test,verbose=2)

print ('validation loss : {} '
       '\naccuracy : {}'.format(loss,acc))
print(30*'#')

prediction_start = timeit.default_timer()
predictions = model.predict_classes(inter_x_test)
prediction_end = timeit.default_timer()
print('Prediction duration : {} seconds'.format(prediction_end-prediction_start))


matrix = np.zeros((10, 2))

for i, x in enumerate(inter_x_test):

    # Class F
    if predictions[i] == 0 and inter_y_test[i] == 0:
        matrix[0, 0] = matrix[0, 0] + 1

    elif predictions[i] == 0 and inter_y_test[i] != 0:
        matrix[1, 0] = matrix[1, 0] + 1

    elif inter_y_test[i] == 0 and predictions[i] != 0:
        matrix[0, 1] = matrix[0, 1] + 1

    elif inter_y_test[i] != 0 and predictions[i] != 0:
        matrix[1, 1] = matrix[1, 1] + 1

    # Class N
    if predictions[i] == 1 and inter_y_test[i] == 1:
        matrix[2, 0] = matrix[2, 0] + 1

    elif predictions[i] == 1 and inter_y_test[i] != 1:
        matrix[3, 0] = matrix[3, 0] + 1

    elif inter_y_test[i] == 1 and predictions[i] != 1:
        matrix[2, 1] = matrix[2, 1] + 1

    elif inter_y_test[i] != 1 and predictions[i] != 1:
        matrix[3, 1] = matrix[3, 1] + 1

    # Class S
    if predictions[i] == 2 and inter_y_test[i] == 2:
        matrix[4, 0] = matrix[4, 0] + 1

    elif predictions[i] == 2 and inter_y_test[i] != 2:
        matrix[5, 0] = matrix[5, 0] + 1

    elif inter_y_test[i] == 2 and predictions[i] != 2:
        matrix[4, 1] = matrix[4, 1] + 1

    elif inter_y_test[i] != 2 and predictions[i] != 2:
        matrix[5, 1] = matrix[5, 1] + 1

    # Class V
    if predictions[i] == 3 and inter_y_test[i] == 3:
        matrix[6, 0] = matrix[6, 0] + 1

    elif predictions[i] == 3 and inter_y_test[i] != 3:
        matrix[7, 0] = matrix[7, 0] + 1

    elif inter_y_test[i] == 3 and predictions[i] != 3:
        matrix[6, 1] = matrix[6, 1] + 1

    elif inter_y_test[i] != 3 and predictions[i] != 3:
        matrix[7, 1] = matrix[7, 1] + 1

    # Class Q
    if predictions[i] == 4 and inter_y_test[i] == 4:
        matrix[8, 0] = matrix[8, 0] + 1

    elif predictions[i] == 4 and inter_y_test[i] != 4:
        matrix[9, 0] = matrix[9, 0] + 1

    elif inter_y_test[i] == 4 and predictions[i] != 4:
        matrix[8, 1] = matrix[8, 1] + 1

    elif inter_y_test[i] != 4 and predictions[i] != 4:
        matrix[9, 1] = matrix[9, 1] + 1

print (matrix)


# import pandas as pd
# import seaborn as sn

# data={'True Lable':    inter_y_test[0],
#         'Predict Lable': predictions[0]
#         }
# print(data)
# df = pd.DataFrame(data,index=[0,1,2,3,4], columns=['True Lable','Predict Lable'])
# confusion_matrix = pd.crosstab(df['True Lable'], df['Predict Lable'], rownames=['True Lable'], colnames=['Predict Lable'], margins = True)
# sn.heatmap(confusion_matrix, annot=True)
# plt.show() 




matrix2 = np.zeros((5,5))
for i, x in enumerate(inter_x_test):
    for index in range(0,5):
        if predictions[i] == index:
            if inter_y_test[i] == 0:
                matrix2[index, 0] = matrix2[index, 0] + 1
            elif inter_y_test[i] == 1:
                matrix2[index, 1] = matrix2[index, 1] + 1
            elif inter_y_test[i] == 2:
                matrix2[index, 2] = matrix2[index, 2] + 1
            elif inter_y_test[i] == 3:
                matrix2[index, 3] = matrix2[index, 3] + 1
            elif inter_y_test[i] == 4:
                matrix2[index, 4] = matrix2[index, 4] + 1

print (matrix2)


print ('\n{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^10}\t{:^10}\t{:^10}\t{:^10}'.format('Class', 'F', 'N', 'S', 'V',
                                                                                           'Q', 'acc', 'Sen', 'Spac',
                                                                                           'PPV'))
print ('----------------------------------------------------------------------------------------------------------------')
print ('\n{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}'.format('F',
                                                               matrix2[0,0],matrix2[0,1],matrix2[0,2],matrix2[0,3],matrix2[0,4],
                                                               ((matrix[0, 0] + matrix[1, 1]) / (matrix[0, 0] + matrix[0, 1] + matrix[1, 0] +matrix[1, 1]))*100,
                                                               (matrix[0, 0] / (matrix[0, 0] + matrix[1, 0]))*100,
                                                               (matrix[1, 1] / (matrix[1, 1] + matrix[0, 1]))*100,
                                                               (matrix[0, 0] / (matrix[0, 0] + matrix[0, 1]))*100))
print ('----------------------------------------------------------------------------------------------------------------')
print ('\n{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}'.format('N',
                                                                matrix2[1,0],matrix2[1,1],matrix2[1,2],matrix2[1,3],matrix2[1,4],
                                                                ((matrix[2, 0] + matrix[3, 1]) / (matrix[2, 0] + matrix[2, 1] + matrix[3, 0] +matrix[3, 1]))*100,
                                                                (matrix[2, 0] / (matrix[2, 0] + matrix[3, 0]))*100,
                                                                (matrix[3, 1] / (matrix[3, 1] + matrix[2, 1]))*100,
                                                                (matrix[2, 0] / (matrix[2, 0] + matrix[2, 1]))*100))
print ('----------------------------------------------------------------------------------------------------------------')
print ('\n{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}'.format('S',
                                                                matrix2[2,0],matrix2[2,1],matrix2[2,2],matrix2[2,3],matrix2[2,4],
                                                                ((matrix[4, 0] + matrix[5, 1]) / (matrix[4, 0] + matrix[4, 1] + matrix[5, 0] +matrix[5, 1]))*100,
                                                                (matrix[4, 0] / (matrix[4, 0] + matrix[5, 0]))*100,
                                                                (matrix[5, 1] / (matrix[5, 1] + matrix[4, 1]))*100,
                                                                (matrix[4, 0] / (matrix[4, 0] + matrix[4, 1]))*100))
print ('----------------------------------------------------------------------------------------------------------------')
print ('\n{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}'.format('V',
                                                                matrix2[3,0],matrix2[3,1],matrix2[3,2],matrix2[3,3],matrix2[3,4],
                                                                ((matrix[6, 0] + matrix[7, 1]) / (matrix[6, 0] + matrix[6, 1] + matrix[7, 0] +matrix[7, 1]))*100,
                                                                (matrix[6, 0] / (matrix[6, 0] + matrix[7, 0]))*100,
                                                                (matrix[7, 1] / (matrix[7, 1] + matrix[6, 1]))*100,
                                                                (matrix[6, 0] / (matrix[6, 0] + matrix[6, 1]))*100))
print ('----------------------------------------------------------------------------------------------------------------')
print ('\n{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}\t{:^10.2f}'.format('Q',
                                                                matrix2[4,0],matrix2[4,1],matrix2[4,2],matrix2[4,3],matrix2[4,4],
                                                                ((matrix[8, 0] + matrix[9, 1]) / (matrix[8, 0] + matrix[8, 1] + matrix[9, 0] +matrix[9, 1]))*100,
                                                                (matrix[8, 0] / (matrix[8, 0] + matrix[9, 0]))*100,
                                                                (matrix[9, 1] / (matrix[9, 1] + matrix[8, 1]))*100,
                                                                (matrix[8, 0] / (matrix[8, 0] + matrix[8, 1]))*100))
print ('----------------------------------------------------------------------------------------------------------------')




